import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule,Routes } from '@angular/router';
import { HttpClientModule,HttpClient,} from '@angular/common/http'
import { HttpModule} from '@angular/http';
import { FormsModule} from '@angular/forms';
import { AppComponent } from './app.component';
import { SocialLoginModule,GoogleLoginProvider,AuthServiceConfig} from "angular-6-social-login";
import { HomePageComponent } from './home-page/home-page.component';
import { LoginPageComponent } from './login-page/login-page.component';

import { HomeloginconnectService } from './homeloginconnect.service';

const route:Routes=[
  {
    path:'',
    component:LoginPageComponent
  },
  {
    path:'homepage',
    component:HomePageComponent,
    canActivate:[HomeloginconnectService]
  },
  {
    path:'login',
    component:LoginPageComponent
  }
]
export function getAuthServiceConfigs() {
  let config = new AuthServiceConfig(
      [
        
        {
          id: GoogleLoginProvider.PROVIDER_ID,
          provider: new GoogleLoginProvider("105262630196-bghcv1kp7qbttv5qlkk07dhfri4lk63s.apps.googleusercontent.com")
        },
          
      ]
  );
  return config;
}

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    LoginPageComponent
  ],
  imports: [
    BrowserModule,SocialLoginModule,
    RouterModule.forRoot(route),
    HttpClientModule,
    HttpModule,
    FormsModule
  ],
  providers: [{
    provide: AuthServiceConfig,
    useFactory: getAuthServiceConfigs
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }

