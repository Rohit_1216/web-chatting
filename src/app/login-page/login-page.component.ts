import { Component, OnInit } from '@angular/core';
import { AuthService, GoogleLoginProvider, SocialLoginModule, SocialUser } from 'angular-6-social-login'
import { Router, Routes } from '@angular/router'

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {
  userData:string
  constructor(public socialAuthService: AuthService, private route: Router) { }
  //google sign in
  public socialSignIn(socialPlatform: string) {
    let socialPlatformProvider;
    if (socialPlatform == "google") {
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    }
    //authorization using library
    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => {
        console.log(socialPlatform + " sign in data : ", userData);
       localStorage.setItem("googledata",JSON.stringify(userData))
       this.route.navigate(["homepage"])
      
      }
    );
  }

  ngOnInit() {
   

}
}
