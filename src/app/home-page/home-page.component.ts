import { Component, OnInit } from '@angular/core';
import { HomeloginconnectService } from '../homeloginconnect.service'
import { element } from '@angular/core/src/render3/instructions';
import { Router } from '@angular/router'
@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {
  channelName: string;
  channel: string;
  Messages: string;
  adduser: string;
  messageArray = [];
  list: any;
  allChannels = [];
  result: string;
  identity: string
  username: string
  userid: string

  log() {
    localStorage.clear()
    this.route.navigate(["login"])
  }

  constructor(private services: HomeloginconnectService, private route: Router) { }

  createChannels() {
    this.services.newChannel(this.channelName).subscribe(channeldata => { console.log("YOUR CHANNEL" + JSON.stringify(channeldata)) }),
      err => { console.log(err) }
  }

  sendUserMessage() {
    this.services.userMsg(this.Messages, this.identity).subscribe(msgData => { console.log("my msg   " + JSON.parse(msgData._body).body) }),
      err => { console.log(err) }
    this.showUserMsg(this.packa);
  }

  packa;

  showUserMsg(packa1) {

    console.log("calles", packa1)
    this.packa = packa1;
    this.services.getUserMsg(packa1).subscribe(msgData => {

      for (let index = 0; index < (JSON.parse(msgData._body).messages.length); index++) {
        this.messageArray[index] = { msgs: JSON.parse(msgData._body).messages[index].body,senderid:JSON.parse(msgData._body).messages[index].from }
      } err => {
        console.log(err);
      }
    })
    this.messageArray = [];
  }

  addMembers() {
    this.services.joinchannel(this.identity,this.result).subscribe(joinData => {
      console.log("helllo" + joinData);
    })
  }
  condition: boolean;
  searchGroup() {
    this.services.searchChannel().subscribe(searchData => {


      for (let i = 0; i < JSON.parse(searchData._body).channels.length; i++) {
        if ((JSON.parse(searchData._body).channels[i].unique_name.toLowerCase()) == this.channel.toLowerCase()) {
          this.condition = true;



          this.result = this.channel;
          break;
        }
        else {

          this.condition = false;
          this.result = "wrong input";
        }
      }
    })

  };
  myChannelArray = [];
  uni = [];
  channelslist() {
    this.services.usersubscribed().subscribe(show => {
      var len = JSON.parse(show._body).channels.length
      for (let index = 0; index < len; index++) {


        this.uni.push(JSON.parse(show._body).channels[index].channel_sid);

      }
      this.channelname(this.uni);
    })

  }


  channelname(uni) {
    console.log("len", uni.length);
    for (let index = 0; index < uni.length; index++) {
      this.services.channelname(uni[index]).subscribe(fetch => {

        this.myChannelArray.push({ name: JSON.parse(fetch._body).unique_name, id: this.uni[index] });
      })
    }


  }

  ngOnInit() {
    this.showUserMsg(this.packa);

    let data = JSON.parse(localStorage.getItem('googledata'))
    this.identity = data.id
    this.username = data.name
    this.channelslist()




  }

}

