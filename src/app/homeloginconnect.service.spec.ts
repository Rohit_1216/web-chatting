import { TestBed, inject } from '@angular/core/testing';

import { HomeloginconnectService } from './homeloginconnect.service';

describe('HomeloginconnectService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HomeloginconnectService]
    });
  });

  it('should be created', inject([HomeloginconnectService], (service: HomeloginconnectService) => {
    expect(service).toBeTruthy();
  }));
});
