import { Injectable, Input } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable, identity } from '../../node_modules/rxjs';
import { map } from '../../node_modules/rxjs/operators';
import { CanActivate,Router} from '@angular/router'
@Injectable({
  providedIn: 'root'
})
export class HomeloginconnectService {

  constructor(private httpclient: Http, private route:Router) { }

  canActivate(){
    if(localStorage.getItem("googledata")){
      return true;
    }
    else{
      console.log("false")
      this.route.navigate(["/"])
      return false
          
    }
  }
  
  
  
  //variables used for API hit twilio
  chatServiceSID: string = "ISa539156e08314e42b1a5c7a0985af407";

  baseUrl: string = "https://chat.twilio.com/v2/Services/";

  auth: string = 'Basic QUMwMGUxMmQwNDFjMDVkN2MzZDBkMzBjZjMwZjNiMGVkNzplMmRjMmU1NGNlNjNjMmJhZGEyMGJjN2ZiODc0NTE2OA==';

  contentType: string = 'application/x-www-form-urlencoded'

  headData = new Headers({ 'Content-Type': this.contentType, 'Authorization': this.auth })

  headerpass = new RequestOptions({ headers: this.headData });

  data = JSON.parse(localStorage.getItem('googledata'))
  userid = (this.data).id
  
 
  channelSID: string = " CH88d4728b250849e386d9934039e27212"

  url = this.baseUrl + this.chatServiceSID + "/Channels/" + this.channelSID ;

  //creating channel/group 
  newChannel(channelName): Observable<any> {
    return this.httpclient.post(this.baseUrl + this.chatServiceSID + "/Channels", "FriendlyName=johndoe&UniqueName=" + channelName, this.headerpass)
  };
  
  //updaet channel name
  updateChannel(newName): Observable<any> {
    return this.httpclient.post(this.baseUrl + this.chatServiceSID + "/Channels/" , this.headerpass)
  };

  
  //sending msg into the channel
  userMsg(Messages,id): Observable<any> {

    return this.httpclient.post(this.url + "/Messages", "Body=" + Messages +"&From="+ id , this.headerpass)
  }

  //Rendering channel msgs on Html
  getUserMsg(test):any {
    this.channelSID=test;
    return this.httpclient.get(this.baseUrl + this.chatServiceSID + "/Channels/" +test+"/Messages", this.headerpass)
  }
  //show all channel
  getChannelList():any{
    return this.httpclient.get(this.baseUrl + this.chatServiceSID + "/Channels",this.headerpass).pipe(map(data=>data))
  }

  //search channel
  searchChannel():any{
    return this.httpclient.get(this.baseUrl + this.chatServiceSID + "/Channels",this.headerpass).pipe(map(searchlist=>searchlist))

  }
 
  //join channel
  joinchannel(id,channelName){
    return this.httpclient.post(this.baseUrl + this.chatServiceSID + "/Channels/"+channelName+"/Members/" ,"Identity="+id+"&ServiceSid="+this.chatServiceSID,this.headerpass)
  }

  //list all memebrs of channel
  listAllMembers():any{
    return this.httpclient.get(this.url+ "/Members" ,this.headerpass).pipe(map(data=>data))
  }
  usersubscribed():any{
    return this.httpclient.get(this.baseUrl+this.chatServiceSID+"/Users/"+this.userid+"/Channels",this.headerpass).pipe(map(usrchnnl=>usrchnnl))
  }
   channelname(pack):any{
     return this.httpclient.get(this.baseUrl+this.chatServiceSID+"/Channels/"+pack,this.headerpass).pipe(map(name=>name))
   }
  }






